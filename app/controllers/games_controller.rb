class GamesController < ApplicationController
  def index
    @games = Game.all
  end

  def show
    @game = Game.find(params[:id])
    @score = ScoreCalculator.new(@game).calculate
  end

  def new
    @game = Game.new
  end

  def create
    @frame_loader = FrameLoader.new(games_params)
    if @frame_loader.parse
      redirect_to @frame_loader.game
    end
  end

  private
    def games_params
      params.require(:game).permit(:name, :pins)
    end
end
