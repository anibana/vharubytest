class Frame < ActiveRecord::Base
  belongs_to :game

  serialize :bonus_shots, Array

  before_save :normalize_values

  def strike?
    self.first_ball == 10
  end

  def spare?
    !self.strike? && (self.score) == 10
  end

  def score
    self.first_ball + self.second_ball + self.bonus_shots.inject(0, :+)
  end

  private
    def normalize_values
      self.first_ball = 0 if self.first_ball.to_i > 10
      self.second_ball = 0 if self.second_ball.to_i > 10
    end
end
