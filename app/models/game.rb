class Game < ActiveRecord::Base
  validates :name, presence: true

  has_many :frames
end
