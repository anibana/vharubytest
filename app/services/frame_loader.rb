class FrameLoader
  attr_accessor :pins, :game

  FRAMES_COUNT = 10

  def initialize(options = {})
    @pins = options[:pins].split(" ") || []
    @game = Game.create(name: options[:name])
  end

  def parse
    ball_number = 0

    FRAMES_COUNT.times do |i|
      current_ball = @pins[ball_number].to_i || 0
      next_ball = @pins[ball_number + 1].to_i || 0
      third_ball = @pins[ball_number + 2].to_i || 0

      if current_ball == 10 #STRIKE
        @game.frames.create(number: i+1, first_ball: current_ball, bonus_shots: [next_ball, third_ball])
        ball_number += 1
      elsif (current_ball + next_ball) == 10 #SPARE
        @game.frames.create(number: i+1, first_ball: current_ball, second_ball: next_ball, bonus_shots: [third_ball])
        ball_number += 2
      else
        @game.frames.create(number: i+1, first_ball: current_ball, second_ball: next_ball)
        ball_number += 2
      end

      puts "Frame #{i+1} created"
    end

  end
end
