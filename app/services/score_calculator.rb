class ScoreCalculator
  attr_reader :game

  FRAMES_COUNT = 10

  def initialize(game)
    @game = game
  end

  def calculate
    score = 0

    FRAMES_COUNT.times do |i|
      frame = @game.frames.find_by(number: i+1)
      score += frame.score rescue 0
    end

    score
  end
end
