Rails.application.routes.draw do
  root to: 'games#index'

  resources :games, except: [:update, :edit, :destroy]
end
