class CreateFrames < ActiveRecord::Migration
  def change
    create_table :frames do |t|
      t.integer :first_ball, default: 0
      t.integer :second_ball, default: 0

      t.timestamps
    end
  end
end
