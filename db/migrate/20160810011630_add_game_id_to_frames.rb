class AddGameIdToFrames < ActiveRecord::Migration
  def change
    add_column :frames, :game_id, :integer
    add_column :frames, :bonus_shots, :text
    add_column :frames, :number, :integer
  end
end
