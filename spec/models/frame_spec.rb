require 'rails_helper'

RSpec.describe Frame, type: :model do
  describe 'default values' do
    subject { Frame.create }
    it { expect(subject.first_ball).to eq 0 }
    it { expect(subject.second_ball).to eq 0 }

    context 'when passed values are greater than 10' do
      it 'sets to 0' do
        frame = Frame.create(first_ball: 11, second_ball: 14)
        expect(frame.first_ball).to eql 0
        expect(frame.second_ball).to eql 0
      end
    end
  end

  describe '#strike?' do
    it 'returns true if first ball is 10' do
      frame = Frame.new(first_ball: 10)

      expect(frame.strike?).to eql true
    end

    it 'returns false if first ball is not 10' do
      frame = Frame.new(first_ball: 9)

      expect(frame.strike?).to eql false
    end
  end

  describe '#spare?' do
    it 'returns true if the total of 2 balls is 10' do
      frame = Frame.new(first_ball: 2, second_ball: 8)

      expect(frame.spare?).to eql true
    end

    it 'returns false if the first ball is 10' do
      frame = Frame.new(first_ball: 10)

      expect(frame.spare?).to eql false
    end

    it 'returns false if the total of 2 balls is not 10' do
      frame = Frame.new(first_ball: 1, second_ball: 1)

      expect(frame.spare?).to eql false
    end
  end

  describe '#score' do
    it 'returns the total of 2 balls' do
      frame = Frame.new(first_ball: 2, second_ball: 5)

      expect(frame.score).to eql 7
    end
  end
end
