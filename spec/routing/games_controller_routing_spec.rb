require 'rails_helper'

RSpec.describe GamesController, type: :routing do
  it { expect(get('/')).to route_to('games#index') }
  it { expect(get('/games')).to route_to('games#index') }
  it { expect(get('/games/1')).to route_to('games#show', id: '1') }
  it { expect(get('/games/new')).to route_to('games#new') }
  it { expect(post('/games')).to route_to('games#create') }

  it { expect(get('/games/1/edit')).to_not route_to('games#edit', id: '1') }
  it { expect(put('/games/1')).to_not route_to('games#update', id: '1') }
  it { expect(delete('/games/1')).to_not route_to('games#destroy', id: '1') }
end
