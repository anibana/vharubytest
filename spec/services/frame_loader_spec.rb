require 'rails_helper'

RSpec.describe FrameLoader do
  describe '#initialize' do
    it 'parse input into array of numbers' do
      pins = "10 10 10"
      loader = FrameLoader.new(pins: pins, name: "game_name")

      expect(loader.pins).to eql ["10", "10", "10"]
    end

    it 'creates game' do
      pins = "10 10 10"
      loader = FrameLoader.new(pins: pins, name: "game_name")

      expect(loader.game.name).to eql "game_name"
    end

    it 'creates loads frames to game' do
      pins = "10 10 10 10"
      loader = FrameLoader.new(pins: pins, name: "game_name")
      loader.parse

      game = loader.game

      expect(game.frames.count).to eql 10
    end
  end
end
