require 'rails_helper'

RSpec.describe ScoreCalculator do
  describe '#initialize' do
    it 'sets game' do
      loader = FrameLoader.new(pins: "10 10 10", name: "new_game")
      loader.parse

      calculator = ScoreCalculator.new(loader.game)

      expect(calculator.game).to eql loader.game
    end
  end

  describe '#calculate' do
    context "when no strikes or spares" do
      it "adds only the frame's score" do
        loader = FrameLoader.new(pins: "1 2 3 4", name: "new_game")
        loader.parse

        calculator = ScoreCalculator.new(loader.game)

        expect(calculator.calculate).to eq 10
      end
    end

    context "when there is spare" do
      it "adds all the frame's score + bonus" do
        loader = FrameLoader.new(pins: "9 1 9 1", name: "new_game")
        loader.parse

        calculator = ScoreCalculator.new(loader.game)

        expect(calculator.calculate).to eql 29
      end
    end

    context "when there is a strike" do
      it "adds all the frame's score + bonus" do
        loader = FrameLoader.new(pins: "1 1 1 1 10 1 1", name: "new_game")
        loader.parse

        calculator = ScoreCalculator.new(loader.game)

        expect(calculator.calculate).to eql 18
      end
    end

    context "when perfect game" do
      it "adds all the frame's score + bonus" do
        loader = FrameLoader.new(pins: "10 10 10 10 10 10 10 10 10 10 10 10", name: "new_game")
        loader.parse

        calculator = ScoreCalculator.new(loader.game)

        expect(calculator.calculate).to eql 300
      end
    end
  end
end
